(function ($) {
	'use strict';
	var ReportKind = {
		TimeSheet: {url: "rest/lastweeks", handler: loadTimeSheetRpt, name: "Quadro de Apropriação"},
		WorkItems: {url: "rest/workitems", handler: loadWorkItems, name: "Tarefas Abertas"}
	};
	var results;
	var hasResults = false;
	var mainButtonGroup;
	var btnRefresh;
	var btnPrevPage;
	var btnNextPage;
	var btnTimeSheetRpt;
	var btnWorkItems;
	var progressPanel;
	var formTimeSheetOptions;
	var cbFormatTime;
	var userName;
	var password;
	var page = 0;
	var reportKind = ReportKind.TimeSheet;
	var cbSumUstNotCompleted;

	$(function () {
		results = $('#results');
		mainButtonGroup = $('#mainButtonGroup');
		btnRefresh = $('#btnRefresh');
		btnPrevPage = $('#btnPrevPage');
		btnNextPage = $('#btnNextPage');
		btnTimeSheetRpt = $('#btnTimeSheetRpt');
		btnWorkItems = $('#btnWorkItems');
		progressPanel = $('#progressPanel');
		formTimeSheetOptions = $('#formTimeSheetOptions');
		cbFormatTime = $('#cbFormatTime');
		cbSumUstNotCompleted = $('#cbSumUstNotCompleted');
		if (localStorage['formatTime'] == 'false') {
			cbFormatTime.prop('checked', false);
		}
		cbFormatTime.change(onCbFormatTimeChange);

		if (localStorage['sumUstNotCompleted'] == 'false') {
			cbSumUstNotCompleted.prop('checked', false);
		}
		cbSumUstNotCompleted.change(oncbSumUstNotCompletedChange);

		$(document).ajaxError(function (/*event, jqxhr, settings, thrownError*/) {
			onError();
		});
		
		showLogin();
		btnRefresh.click(function () {
			refresh(); // coloca em uma função porque refresh não pode receber o evento como parâmetro.
		});
		btnPrevPage.click(prevPage);
		btnNextPage.click(nextPage);
		btnTimeSheetRpt.click(showTimeSheetRpt);
		btnWorkItems.click(showWorkItems);
	});

	function onError(msg) {
		if (hasResults) {
			showResults();
		} else {
			showLogin();
		}
		alert(msg || "Não foi possível completar a operação.");
	}

	function onCbFormatTimeChange() {
		localStorage['formatTime'] = cbFormatTime.prop("checked");
		refresh();
	}
	
	function oncbSumUstNotCompletedChange() {
		localStorage['sumUstNotCompleted'] = cbSumUstNotCompleted.prop("checked");
		refresh();
	}

	function showTimeSheetRpt() {
		refresh(ReportKind.TimeSheet);
	}

	function showWorkItems() {
		refresh(ReportKind.WorkItems);
	}

	function showLogin() {
		results.html('');
		progressPanel.hide();

		var edPass = $("#edPassword");
		var edUserName = $("#edUserName");
		$('#loginPanel').show();
		var lastUser = localStorage['lastUser'];
		if (lastUser) {
			edUserName.val(lastUser);
			edPass.focus();
		} else {
			edUserName.focus();
		}
		edUserName.closest("form:not(.ok)").submit(loginSubmit).addClass('ok');
	}

	function loginSubmit() {
		userName = $.trim($("#edUserName").val());
		password = $("#edPassword").val();
		if (userName == '' || password == '') {
			return false;
		}
		localStorage['lastUser'] = userName;
		$('#loginPanel').hide();
		refresh();
		return false;
	}

	function refresh(newReportKind) {
		if (!newReportKind) {
			newReportKind = reportKind;
		}
		results.hide();
		mainButtonGroup.hide();
		progressPanel.find(".panel-title").text(newReportKind.name);
		progressPanel.show();
		var formatTime = cbFormatTime.prop("checked");
		var sumUstNotCompleted = cbSumUstNotCompleted.prop("checked");
		$.ajax(newReportKind.url, {
			data: {'user': userName, 'password': password, 'page': page, 'formatTime': formatTime, 'sumUstNotCompleted': sumUstNotCompleted},
			type: 'POST',
			newReportKind: newReportKind,
			success: processResp
		});
		updateButtons();
	}

	function nextPage() {
		gotoPage(-1);
	}

	function prevPage() {
		gotoPage(1)
	}

	function gotoPage(relative) {
		page = Math.max(0, page + relative);
		refresh();
	}

	function updateButtons() {
		btnRefresh.show();
		var progressVisible = progressPanel.is(":visible");
		if (reportKind.url == ReportKind.TimeSheet.url) {
			if (page == 0) {
				btnNextPage.addClass("disabled").prop('disabled', true);
			} else {
				btnNextPage.removeClass("disabled").prop('disabled', false);
			}
			btnPrevPage.show();
			btnNextPage.show();
			btnTimeSheetRpt.hide();
			btnWorkItems.show();
			if (!progressVisible) {
				formTimeSheetOptions.show();
			}
		} else {
			btnPrevPage.hide();
			btnNextPage.hide();
			btnWorkItems.hide();
			btnTimeSheetRpt.show();
			formTimeSheetOptions.hide();
		}
		if (progressVisible) {
			mainButtonGroup.hide();
			formTimeSheetOptions.hide();
		} else {
			mainButtonGroup.show();
		}
	}

	function processResp(resp) {
		var data = resp.data;
		if (data.error) {
			onError(data.error);
		} else {
			reportKind = this.newReportKind;
			loadResults(data);
		}
	}

	function showResults() {
		progressPanel.hide();
		results.show();
		updateButtons();
	}

	function loadResults(data) {
		results.html('');
		hasResults = true;
		showResults();
		if (data.result.length == 0) {
			$('<h2/>').text('Nenhum registro encontrado.').appendTo(results);
		} else {
			reportKind.handler(data);
		}
	}

	function loadWorkItems(data) {
		$('<div class="page-header"><h2>' + reportKind.name + '</h2></div>').appendTo(results);
		for (var p = 0; p < data.result.length; p++) {
			var proj = data.result[p];
			$('<h4>' + proj.name + '</h4>').appendTo(results);

			var workitems = $('<div class="workitem-list"/>');
			for (var i = 0; i < proj.workItems.length; i++) {
				var parent = proj.workItems[i];

				var row = $('<div class="row"/>');
				var parents = $('<div class="parents col-sm-4"/>');
				row.append(parents);
				if (parent.name) {
					parents.append(loadItem(parent));
				}
				var children = $('<div class="children col-sm-8"/>');
				row.append(children);
				for (var j = 0; j < parent.children.length; j++) {
					children.append(loadItem(parent.children[j]));
				}
				workitems.append(row);
			}

			results.append(workitems);
		}

		function loadItem(item) {
			var wi = $('<div class="workitem"/>');
			if (item.type) {
				wi.addClass(item.type);
			}
			if (!item.mine) {
				wi.addClass('not-mine');
			}
			var id = $('<span class="id"/>').text(item.id);
			var name = $('<span class="name"/>').text(item.name);
			wi.append($('<a/>').append(id).append(name).attr('href', item.url || '#').attr('target', '_blank'));
			return wi;
		}
	}

	function loadTimeSheetRpt(data) {
		var row;
		results.css('visibility', 'hidden');
		var maxHeight = 0, days = [];
		var totalUST = 0;
		var wiCountUST = [];
		for (var i = 0; i < data.result.length; i++) {
			var item = data.result[i];
			if (i % 7 == 0) {
				row = $('<div class="row"/>');
				results.append(row);
			}
			var daySummary = $('<div class="col-sm-1-7 day-summary"/>');
			days.push(daySummary[0]);
			row.append(daySummary);
			if (item.future) {
				daySummary.addClass('future');
			}
			if (item.workday) {
				daySummary.addClass('workday');
			}
			if (item.absences.length > 0) {
				daySummary.addClass('absence');
			}
			if (item.workitems.length == 0) {
				daySummary.addClass('empty');
			}
			daySummary.append($('<span class="week-day"/>').text(item.desc));
			if (item.workday || item.total != '0' || item.worktotal) {
				var total = $('<span class="total"/>').text(item.total);
				daySummary.append(total);
				if (item.worktotal) {
					var worktotal = $('<span class="worktotal"/>').text(item.worktotal);
					if (item.workratio) {
						if (item.workratio > 1) {
							worktotal.addClass('ratio-wrong')
						} else if (item.workratio < 0.95) {
							worktotal.addClass('ratio-bad')
						} else {
							worktotal.addClass('ratio-good')
						}
					}
					total.append(worktotal);
				}
			}
			var workitems = $('<div class="workitems"/>');
			$.each(item.workitems, function () {
				var wi = $('<span class="workitem"/>');
				wi.append($('<a/>').text(this.id).attr('title', this.name).attr('href', this.url || '#')
					.attr('target', '_blank'));
				wi.append($('<span class="daytotal"/>').text(this.total));
				if (!this.ustItem){
					wi.append($('<span class="daytotal"/>').text("*").attr('title', "Código de UST não encontrado"));
				}
				workitems.append(wi);

				 var sumTotalUST = cbSumUstNotCompleted.prop("checked") || this.internalState == "CONCLUIDO";

			        if (!wiCountUST.includes(this.id) && sumTotalUST) {
          				totalUST += this.ustItem ? this.ustItem.value : 0;
			                wiCountUST.push(this.id);
			        }
			});
			$.each(item.absences, function () {
				var wi = $('<span class="workitem"/>');
				wi.append($('<span class="description"/>').text(this.name));
				wi.append($('<span class="daytotal"/>').text(this.total));
				workitems.append(wi);
			});
			daySummary.append(workitems);
			
			
			daySummary.append($('<span class="total-ust-day"/>').text("UST: " + item.totalUst));


			var itemHeight = daySummary.height() + item.workitems.length + 20;
			if (maxHeight < itemHeight) {
				maxHeight = itemHeight;
			}
		}		

		$(days).height(maxHeight);
		results.css('visibility', '');
		results.append($('<div class="total-ust-periodo"/>').text("Total UST no Período: " + totalUST));

	}
})(jQuery);
