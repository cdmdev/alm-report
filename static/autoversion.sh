#!/bin/bash

if [ ! -d "$1" ]; then
    echo "Supply a valid directory as argument."
	exit 1
fi

basedir=$(cd "$1"; pwd)
sedcmd=""
autoversions="$basedir/autoversions"

# remove old versions
if [ -f $autoversions ]; then
	for f in $(cat $autoversions); do
		echo "Removing $basedir$f"
		rm -f "$basedir$f"
	done
	rm -f $autoversions
fi

for f in $(find $basedir -iname '*.css' -o -iname '*.js'); do
	if [ -n "$(hg locate $f)" ]; then
		echo "Ignoring versioned file $f"
		continue
	fi
	cs=$(md5sum $f | awk '{print $1}')
	dir=$(dirname $f)
	name=$(basename $f)
	newname="$cs.$name"
	newpath="$dir/$newname"
	echo "$newpath"
	cp $f $newpath
	# escape names
	ename=$(echo $name | sed -e 's/[]\/$*.^|[]/\\&/g')
	enewname=$(echo $newname | sed -e 's/[\/&]/\\&/g')
	sedcmd="$sedcmd -e s/\(['\"/]\)$ename/\1$enewname/g"
	echo ${newpath#$basedir} >> "$autoversions"
done

if [ -n "$sedcmd" ]; then
	sedcmd="sed -i $sedcmd"
	find $basedir \( -iname '*.js' -o -iname '*.html' -o -iname '.xhtml' \) -exec $sedcmd '{}' ';'
fi
