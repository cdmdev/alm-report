library date;

class Date extends DateTime {
  Date(int year, int month, int day) : super.utc(year, month, day);

  Date.fromDateTime(DateTime datetime) : this(datetime.year, datetime.month, datetime.day);

  Date.now() : this.fromDateTime(new DateTime.now());

  DateTime toLocal() {
    return new DateTime(this.year, this.month, this.day);
  }

  Date subtract(Duration duration) {
    assert(duration.inMicroseconds % Duration.microsecondsPerDay == 0);
    return new Date.fromDateTime(super.subtract(duration));
  }

  Date add(Duration duration) {
    assert(duration.inMicroseconds % Duration.microsecondsPerDay == 0);
    return new Date.fromDateTime(super.add(duration));
  }
}
