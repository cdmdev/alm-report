library config;

import 'dart:async';
import 'dart:io';
import 'package:dartdap/dartdap.dart';
import 'package:yaml/yaml.dart';
import 'package:logging/logging.dart';

import "date.dart";

Logger logger = new Logger("config");

YamlMap _config;

Future<YamlMap> loadConfig([String filename = "config.yaml"]) {
  return new File(filename).readAsString().then((filecontent) {
    _config = loadYaml(filecontent);
    return _config;
  });
}

Future<Iterable<Date>> getHolydays() {
  //TODO
  return Future.value([]);
}

Map<String, int> _matriculaCache = {};

Future<int> getMatricula(String cpf) {
  var cached = _matriculaCache[cpf];
  if (cached != null) {
    return new Future.value(cached);
  }
  YamlMap ldapconf = _config['ldap']['default'];
  var ldapcon = new LdapConnection(
      host: ldapconf['host'],
      bindDN: ldapconf['bindDN'],
      port: ldapconf['port'],
      password: ldapconf['password']);

  var attrs = ["employeeNumber"];
  var filter = Filter.equals("uid", cpf);

  return ldapcon.search("dc=serpro,dc=gov,dc=br", filter, attrs).then((result) {
    return result.stream.toList().then((list) {
      if (list.isEmpty) {
        return null;
      }
      String matricula = list.first.attributes["employeeNumber"].values.first;
      if (matricula == null || matricula.trim() == "") {
        return null;
      }
      _matriculaCache[cpf] = int.parse(matricula);
      return _matriculaCache[cpf];
    });
  }).whenComplete(() {
    ldapcon.close();
  });
}

void main(List<String> args) {
  loadConfig().then((config) {
    getMatricula("03864072905").then((matricula) {
      print(matricula);
    });
  });
}
