#!/usr/bin/python
# -*- coding: utf-8 -*-
# Created: 09-ago-2014
__author__ = 'Israel Rios'

import bsddb3 as bsddb
import multiprocessing
import os

DB_DIR = "/tmp/dbtest"


def main(start):
    if start < 100000:
        p = multiprocessing.Process(target=main, args=(start + 1000,))
        p.start()
    e = bsddb.db.DBEnv()
    e.set_lk_detect(bsddb.db.DB_LOCK_DEFAULT)
    e.open(DB_DIR, bsddb.db.DB_CREATE | bsddb.db.DB_THREAD | bsddb.db.DB_INIT_LOCK | bsddb.db.DB_INIT_MPOOL)
    db = bsddb.db.DB(e)
    db.open(os.path.join(DB_DIR, "test.db"), bsddb.db.DB_BTREE, bsddb.db.DB_CREATE | bsddb.db.DB_THREAD, 0666)
    for i in range(start, start + 1000):
        db.get(str(i-1000))
        db[str(i)] = str(i)
    db.sync()


if __name__ == "__main__":
    if not os.path.exists(DB_DIR):
        os.mkdir(DB_DIR)
    main(0)
