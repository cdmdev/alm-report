#!/usr/bin/python
# -*- coding: utf-8 -*-
# Autor: Israel Rios
# Created: 03-jul-2014
import sys
import locale

locale.setlocale(locale.LC_TIME, "pt_BR.utf-8")

import os.path
import cherrypy
import alm
import datetime


DATE_FORMAT = '%a %d/%b'
TIMEOUT = 30  # segundos
PROCESS_ERROR = u"O processo terminou inexperadamente."
LOGIN_ERROR = u"Não foi possível fazer login no Alm."

curdir = os.path.abspath(os.path.dirname(__file__))
staticdir = os.path.join(curdir, 'static')


def response(data):
    return {'data': data}


def error(msg):
    return response({"error": msg})


def format_millis(millis):
    minutes = millis / 1000 / 60
    return "%02d:%02d" % (int(minutes / 60), minutes % 60)


class AlmReport(object):
    def __init__(self, user, password):
        super(AlmReport, self).__init__()
        self.password = password
        self.user = user

    def run(self):
        results = []
        service = None
        try:
            service = alm.Alm(self.user, self.password)
            service.login()
            today = datetime.date.today()
            daysback = 7 + today.weekday() + 1
            if daysback < 14:
                daysback += 7

            daymap = service.calcTimeSpent(daysback)

            # monta o relatório
            daydelta = datetime.timedelta(days=1)
            i = today - daydelta * daysback
            for _ in range(daysback + 10):  # verifica no futuro também.
                day = daymap.get(i)
                if day is None and i > today:
                    continue
                item = {'desc': i.strftime(DATE_FORMAT), 'total': '0', 'workitems': [], 'future': i > today}
                results.append(item)
                if day is not None:
                    workitems = item['workitems']
                    for w in day.workitems:
                        daytotal = format_millis(w.daytotal(i, self.user))
                        workitems.append({'id': w.id, 'name': w.name, 'url': w.url, 'total': daytotal})
                    item['total'] = format_millis(day.sum)
                i += daydelta
        except alm.LoginException:
            return error(LOGIN_ERROR)
        except:
            cherrypy.log("Error", traceback=True)
            return error(PROCESS_ERROR)
        finally:
            if service is not None:
                service.logout()

        return response({'result': results})


class WebApp(object):
    def __init__(self):
        pass

    @cherrypy.expose
    def index(self):
        return file(os.path.join(staticdir, 'index.html'))

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def lastweeks(self, user, password):
        return AlmReport(user, password).run()


cherrypy.config.update({'engine.autoreload.on': False})

cherrypy_conf = {
    '/': {
        'tools.staticdir.on': True,
        'tools.staticdir.root': staticdir,
        'tools.staticdir.dir': '.'
        # 'tools.sessions.on': True,
        # 'tools.sessions.timeout': 10
    }
}
# Mode detection
if __name__ == "__main__":
    port = 9090
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    cherrypy.config.update({'server.socket_port': port})
    cherrypy.quickstart(WebApp(), '/alm', cherrypy_conf)

elif 'wsgi' in __name__.lower():
    cherrypy.config.update({'environment': 'embedded'})
    application = cherrypy.Application(WebApp(), config=cherrypy_conf)
