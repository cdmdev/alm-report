#!/usr/bin/python
# -*- coding: utf-8 -*-
# Autor: Israel Rios
# Created: 07-ago-2013

import os
import cookielib
import urllib2
import urllib
import datetime
from tagutil import XmlDoc

D_FORMAT = '%Y-%m-%d'
DT_FORMAT = D_FORMAT + 'T%H:%M:%S.%f'
curdir = os.path.abspath(os.path.dirname(__file__))


def parseDate(sdate):
    parts = sdate[:10].split('-')
    return datetime.date(year=int(parts[0]), month=int(parts[1]), day=int(parts[2]))


def tostring(obj):
    if isinstance(obj, unicode):
        return obj
    elif isinstance(obj, str):
        return obj.decode('utf-8')
    else:
        return unicode(obj)


class LoginException(Exception):
    pass


class Monitor(object):
    def login(self):
        pass

    def workitems(self):
        pass


class PrintMonitor(Monitor):
    def login(self):
        print "Login ..."

    def workitems(self):
        print "Loading workitems ..."


class Project(object):
    def __init__(self, _id, name):
        self.name = name
        self.id = _id

    def __repr__(self, *args, **kwargs):
        return (self.id + " - " + self.name).encode('utf8')


class TimeSheetEntry(object):
    def __init__(self, ts, date, userId):
        self.spent = ts
        self.date = date
        self.userId = userId

    def __repr__(self, *args, **kwargs):
        return str(self.spent) + " - " + str(self.date) + " - " + str(self.userId)

    def toMap(self):
        return {'spent': self.spent, 'date': self.date, 'userId': self.userId}


class WorkItem(object):
    def __init__(self, _id='', name='', modified='', url=None):
        self.id = _id
        self.name = name
        self.modified = modified
        self.timesheet = []
        self.url = url

    def daytotal(self, day, userId):
        total = 0
        for entry in self.timesheet:
            if entry.date != day or entry.userId != userId:
                continue
            total += entry.spent
        return total

    @classmethod
    def fromMap(cls, values):
        item = WorkItem(values['id'], values['name'], values['modified'], values['url'])
        for tse in values['timesheet']:
            item.timesheet.append(TimeSheetEntry(tse['spent'], tse['date'], tse['userId']))
        return item

    def toMap(self):
        ts = [tse.toMap() for tse in self.timesheet]
        return {'id': self.id, 'name': self.name, 'modified': self.modified, 'url': self.url, 'timesheet': ts}


class DaySummary(object):
    def __init__(self, date):
        object.__init__(self)
        self.date = date
        self.sum = 0
        self.workitems = []


class Alm(object):
    ccm = u'https://alm.serpro/ccm/'
    urlLogin = ccm + u'auth/j_security_check'
    urlAuthRequired = ccm + u'auth/authrequired'
    ccmService = ccm + u'service/'
    rptDateFormat = "%Y-%m-%dT%H:%M:%S.000-0000"  # 2014-08-18T10:00:00.000-0000 o Timezone for fixado pra facilitar
    urlListProjectsCCM = ccmService + \
                         u'com.ibm.team.process.internal.service.web.IProcessWebUIService/allProjectAreas?userId='
    urlLogout = ccmService + u'com.ibm.team.repository.service.internal.ILogoutRestService'
    urlRptWorkItem = ccm + u'rpt/repository/workitem?size=200&'
    urlShowWorkItem = ccm + u"resource/itemName/com.ibm.team.workitem.WorkItem/"

    def __init__(self, username, password, monitor=None):
        self.cookies = cookielib.CookieJar()  # cookies são necessários para a autenticação
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookies))
        self.setHeaders()
        self.password = password
        self.username = username
        self.monitor = monitor or PrintMonitor()

    def setHeaders(self, _headers=None):
        headers = []
        if _headers is not None:
            headers.extend(_headers)
        self.opener.addheaders = headers

    def login(self):
        self.monitor.login()
        fields = {'j_username': self.username, 'j_password': self.password}
        self.setAjaxHeaders()
        resp = self.opener.open(self.urlLogin, urllib.urlencode(fields))
        if resp.url.startswith(self.urlAuthRequired):
            print resp.url
            raise LoginException()
        return resp

    def logout(self):
        url = self.ajax(self.urlLogout, {})
        url.read()
        # print url.info()

    def getProjects(self):
        # self.monitor.projects()
        url = self.ajax(self.urlListProjectsCCM + self.username)
        doc = XmlDoc(url.read().decode('utf-8'))
        result = []
        for p in doc.first('returnValue').find('values', 1):
            if p.first('archived').text.lower() == 'true':
                continue
            pid = p.first('itemId').text
            result.append(Project(pid, p.first('name').text))
        return result

    def setAjaxHeaders(self, acceptjson=False):
        headers = []
        if acceptjson:
            headers.append(('accept', 'text/json'))
        return self.setHeaders(headers)

    def ajax(self, url, fields=None, acceptjson=False):
        self.setAjaxHeaders(acceptjson)
        if fields is None:
            return self.opener.open(url)
        return self.opener.open(url, urllib.urlencode(fields))

    def calcTimeSpent(self, daysback):
        daymap = {}
        today = datetime.date.today()
        for wi in self.getAllWorkItens(daysback):
            for entry in wi.timesheet:
                # considera somente os últimos daysback dias
                if (today - entry.date).days > daysback or entry.userId != self.username or entry.spent == 0:
                    continue
                day = daymap.get(entry.date)
                if day is None:
                    day = DaySummary(entry.date)
                    daymap[entry.date] = day
                day.sum += entry.spent
                day.workitems.append(wi)
        return daymap

    def getAllWorkItens(self, daysback):
        """
         Retorna os work itens do usuário modificados recentemente.
        """
        self.monitor.workitems()
        mindate = datetime.datetime.utcnow() - datetime.timedelta(days=daysback + 1)
        # service doc:
        # http://open-services.net/pub/Main/ReportingHome/Reportable_Rest_Services_Interfaces-OSLC_Submission.pdf
        # https://jazz.net/wiki/bin/view/Main/ReportsRESTAPI#time_SheetEntry_type_com_ibm_tea
        expression = 'workitem/timeSheetEntry[creator/userId=%s and startDate>%s]/' \
                     '(timeSpent|startDate|creator/userId|workItem/(id|summary))' \
                     % (self.username, mindate.strftime(self.rptDateFormat))
        url = self.urlRptWorkItem + urllib.urlencode({'fields': expression})
        resp = self.opener.open(url, urllib.urlencode({'username': self.username, 'password': self.password}))
        doc = XmlDoc(resp.read().decode('utf-8'))
        workitems = {}
        today = datetime.date.today()
        for winode in doc.find('workItem'):
            widnode = winode.first('id')
            if widnode is None:
                continue
            wid = widnode.text
            witem = workitems.get(wid)
            if witem is None:
                witem = WorkItem(wid)
                witem.name = winode.first('summary').text
                print witem.name.encode('utf-8')
                witem.url = self.urlShowWorkItem + wid
                workitems[wid] = witem

            tsnode = winode.parent
            tse = TimeSheetEntry(int(tsnode.first('timeSpent').text),
                                 parseDate(tsnode.first('startDate').text),
                                 tsnode.first('userId').text.strip())
            print tse.date.strftime(D_FORMAT)
            if tse.date > today and tse.spent > 0:
                print "Time Entry in future: ", wid, tse.date, tse.spent
            witem.timesheet.append(tse)

        return workitems.values()


def showUserReport(user, password):
    print "Init .",
    alm = Alm(user, password)
    print "."
    alm.login()
    try:
        today = datetime.date.today()
        daysback = 7 + today.weekday() + 1
        if daysback < 14:
            daysback += 7
        daymap = alm.calcTimeSpent(daysback)
        print '\nTotals per day:'
        daydelta = datetime.timedelta(days=1)
        i = today - daydelta * daysback
        for _ in range(daysback + 10):  # verifica no futuro também.
            day = daymap.get(i)
            if day is None:
                # ignora sábado, domingo e datas futuras sem apropriação
                if not i.weekday() in (5, 6) and i <= today:
                    print i.strftime("%a %b %d"), datetime.timedelta(milliseconds=0)
            else:
                workitems = ', '.join([w.id for w in day.workitems])
                print i.strftime("%a %b %d"), datetime.timedelta(milliseconds=day.sum), '  \t', workitems
            i += daydelta
    finally:
        alm.logout()


if __name__ == "__main__":
    import getpass

    showUserReport(getpass.getuser(), getpass.getpass())

