package com.rios.alm;

import java.util.Arrays;

public enum InternalState {
    CONCLUIDO("s3", "Concluído"),
    EM_ATENDIMENTO("s2", "Em Atendimento"),
    ABERTO("s1", "Aberto"),
    NAO_MAPEADO("", "Situação mão mapeada");

    private String code;
    private String desc;

    private InternalState(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static InternalState fromCode(String code) {
        return Arrays.asList(values())
            .stream()
            .filter(is -> is.code.equals(code))
            .findFirst()
            .orElse(NAO_MAPEADO);
    }

    public boolean isCompleted() {
        return this == CONCLUIDO;
    }

}
