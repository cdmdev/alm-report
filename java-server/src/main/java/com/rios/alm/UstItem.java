package com.rios.alm;

public class UstItem {

    private String code;
    private int value;

    public UstItem() {
    }

    public UstItem(String code) {
        super();
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
