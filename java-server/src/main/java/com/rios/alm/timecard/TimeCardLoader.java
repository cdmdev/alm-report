package com.rios.alm.timecard;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.rios.alm.exception.RemoteServiceException;

public final class TimeCardLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeCardLoader.class);

    private static final LoadingCache<CacheKey, Map<LocalDate, Integer>> TIMECARDS = CacheBuilder
        .newBuilder()
        .maximumSize(300)
        .concurrencyLevel(2)
        .refreshAfterWrite(1, TimeUnit.HOURS)
        .build(new CacheLoader<CacheKey, Map<LocalDate, Integer>>() {

            @Override
            public Map<LocalDate, Integer> load(CacheKey key) {
                return new SiscopTimeCardSource(key.username, key.password).makeTimeCardMap(key.baseDate);
            }

            @Override
            public ListenableFuture<Map<LocalDate, Integer>> reload(CacheKey key,
                Map<LocalDate, Integer> oldValue) {
                try {
                    return Futures.immediateFuture(load(key));
                } catch (Exception e) {
                    LOGGER.error("", e);
                    return Futures.immediateFuture(oldValue);
                }
            }
        });

    private TimeCardLoader() {
    }

    public static FutureTask<Map<LocalDate, Integer>> getTimeCardAsync(final String username,
        final String password, final LocalDate date, final int daysback) {

        return new FutureTask<>(() -> getTimeCard(username, password, date, daysback));
    }

    private static Map<LocalDate, Integer> getTimeCard(String username, String password, LocalDate date,
        int daysback) {

        LocalDate endDate = date.withDayOfMonth(1);
        LocalDate baseDate = date.minusDays(daysback).withDayOfMonth(1);

        Map<LocalDate, Integer> result = new HashMap<>(25);

        while (!baseDate.isAfter(endDate)) {
            result.putAll(getTimeCard(username, password, baseDate));
            baseDate = baseDate.plusMonths(1);
        }
        return result;
    }

    private static Map<LocalDate, Integer> getTimeCard(final String username, final String password,
        final LocalDate baseDate) {

        CacheKey cacheKey = new CacheKey(username, password, baseDate);
        try {
            return TIMECARDS.get(cacheKey);
        } catch (ExecutionException e) {
            throw new RemoteServiceException(e);
        }
    }

    private static class CacheKey {

        private final String username;
        private final String password;
        private final LocalDate baseDate;

        CacheKey(String username, String password, LocalDate baseDate) {
            super();
            this.username = username;
            this.baseDate = baseDate;
            this.password = password;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof CacheKey)) {
                return false;
            }
            CacheKey o = (CacheKey) obj;
            return o.username.equals(username) && o.password.equals(password) && o.baseDate.equals(baseDate);
        }

        @Override
        public int hashCode() {
            return Objects.hash(username, password, baseDate);
        }
    }
}
