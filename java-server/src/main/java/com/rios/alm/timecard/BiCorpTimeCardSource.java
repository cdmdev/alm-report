package com.rios.alm.timecard;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.rios.alm.exception.LoginException;
import com.rios.alm.http.BasicHttpClient;
import com.rios.alm.http.LoggedInClient;
import com.rios.alm.ldap.Ldap;
import com.rios.alm.util.HtmlUtils;
import com.rios.alm.util.JsonUtils;

class BiCorpTimeCardSource extends LoggedInClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(BiCorpTimeCardSource.class);

    private static final String URL_BASE = "https://bicorporativo.serpro.gov.br/dwcorporativo/";

    private static final String URL_QUERY = URL_BASE + "plugin/cda/api/doQuery";

    private static final String LOGIN_HOST = "login.serpro.gov.br";

    private static final String URL_LOGIN_PAGE = "https://" + LOGIN_HOST;

    private static final String URL_LOGIN = URL_LOGIN_PAGE + "/auth/realms/serpro/login-actions/authenticate";

    private static final BasicHttpClient CLIENT = makeHttpClient();

    BiCorpTimeCardSource(String username, String password) {
        super(CLIENT, username, password);
    }

    private static BasicHttpClient makeHttpClient() {
        return new BasicHttpClient() {
            @Override
            protected void interceptResponse(HttpResponse response, HttpContext context) {
                if (isLoginRequest(response, context)) {
                    throw new LoginException(URL_LOGIN);
                }
                super.interceptResponse(response, context);
            }
        };
    }

    Map<LocalDate, Integer> makeTimeCardMap(LocalDate baseDate) {

        // paramparam_data_siscop:201501
        // paramparam_empregado:Israel Rios
        // paramparam_data:01/2015
        // path:/home/ALM/paineis/projeto/supde/usu_apro.cda
        // dataAccessId:sql_grafico
        // outputIndexId:1
        // pageSize:0
        // pageStart:0

        LOGGER.info("Getting time card for {}. Date: {}", username, baseDate);

        String name = Ldap.getUser(username).getName();

        String pdataSiscop = String.format("%04d%02d", baseDate.getYear(), baseDate.getMonth().getValue());
        String pdata = String.format("%02d/%04d", baseDate.getMonth().getValue(), baseDate.getYear());

        HttpUriRequest request = buildPost(URL_QUERY)
            .addParameter("paramparam_data_siscop", pdataSiscop)
            .addParameter("paramparam_empregado", name)
            .addParameter("paramparam_data", pdata)
            .addParameter("paramcpf_emp", username)
            .addParameter("path", "/home/ALM/paineis/projeto/supde/usu_apro.cda")
            .addParameter("dataAccessId", "sql_grafico")
            .addParameter("outputIndexId", "1")
            .addParameter("pageSize", "0")
            .addParameter("pageStart", "0")
            .build();

        String text = getText(execute(request));

        SiscopChart chart = JsonUtils.fromJson(text, SiscopChart.class);

        Map<LocalDate, Integer> result = new HashMap<>();

        for (Object[] item : chart.resultset) {
            if (((String) item[0]).contains("SISCOP")) {
                LocalDate itemDate = baseDate.withDayOfMonth(Integer.parseInt((String) item[1]));

                // "value" está em horas. Ex.: 7.6
                double value = ((Number) item[2]).doubleValue();
                if (value < (1.0 / 60.0)) {
                    // value é menor que um minuto. Deve ser zero.
                    continue;
                }
                // converte a hora para milisegundos
                result.put(itemDate, (int) (value * 60 * 60 * 1000));
            }
        }
        return result;
    }

    private static String getLocation(HttpResponse response) {
        Header location = response.getFirstHeader("location");
        if (location != null) {
            return location.getValue();
        }
        return null;
    }

    private static URI getLastRedirectLocation(HttpContext context) {
        List<URI> redirectLocations = HttpClientContext.adapt(context).getRedirectLocations();
        if (redirectLocations != null && !redirectLocations.isEmpty()) {
            return redirectLocations.get(redirectLocations.size() - 1);
        }
        return null;
    }

    private static boolean isLoginRequest(HttpResponse response, HttpContext context) {
        URI lastRedirectLocation = getLastRedirectLocation(context);
        if (lastRedirectLocation != null && LOGIN_HOST.equals(lastRedirectLocation.getHost())) {
            return true;
        }
        String location = getLocation(response);
        return location != null && location.startsWith(URL_LOGIN_PAGE);
    }

    @Override
    protected boolean isLoggedIn() {
        return session.getLoginBiCorp() != null;
    }

    @Override
    protected void login() {
        if (session.getLoginBiCorp() != null) {
            LOGGER.info("Login requested. Last login: {}", session.getLoginBiCorp());
        }
        LOGGER.info("Login");
        session.setLoginBiCorp(null);

        final String html;
        try {
            disableResponseInterceptor();
            html = getText(executeLoggedOff(buildGet(URL_BASE).build()));
        } finally {
            enableResponseInterceptor();
        }

        final String formAction = HtmlUtils.getFormUrl(html, "username");

        HttpUriRequest request = buildPost(formAction).addParameter("username", username)
            .addParameter("password", password)
            .build();
        EntityUtils.consumeQuietly(executeLoggedOff(request).getEntity());

        session.setLoginBiCorp(LocalDateTime.now());
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class SiscopChart {
        List<Object[]> resultset = new ArrayList<>();
    }
}
