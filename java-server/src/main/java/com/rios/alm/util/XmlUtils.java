package com.rios.alm.util;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public final class XmlUtils {
    private XmlUtils() {
    }

    public static Document createXmlDocument(String xmltext) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            return db.parse(new InputSource(new StringReader(xmltext)));
        } catch (SAXException | ParserConfigurationException | IOException e) {
            throw new XmlUtilsException(e);
        }
    }

    /**
     * Find the named subnode in a node's sublist.
     * <ul>
     * <li>Ignores comments and processing instructions.
     * <li>Ignores TEXT nodes (likely to exist and contain ignorable whitespace, if not validating.
     * <li>Ignores CDATA nodes and EntityRef nodes.
     * <li>Examines element nodes to find one with the specified name.
     * </ul>
     * 
     * @param name the tag name for the element to find
     * @param node the element node to start searching from
     * @return the Node found
     */
    public static Node findSubNode(String name, Node node) {
        SingleItemCallback singleItemCallback = new SingleItemCallback();
        findRecur(name, node, 0, singleItemCallback);
        return singleItemCallback.value;
    }

    /**
     * Find the named subnode in a node's sublist.
     * <ul>
     * <li>Ignores comments and processing instructions.
     * <li>Ignores TEXT nodes (likely to exist and contain ignorable whitespace, if not validating.
     * <li>Ignores CDATA nodes and EntityRef nodes.
     * <li>Examines element nodes to find one with the specified name.
     * </ul>
     * 
     * @param name the tag name for the element to find
     * @param node the element node to start searching from
     * @return the Node found
     */
    public static Node first(String name, Node node) {
        SingleItemCallback singleItemCallback = new SingleItemCallback();
        findRecur(name, node, Integer.MAX_VALUE, singleItemCallback);
        return singleItemCallback.value;
    }

    /**
     * Search nodes by name.
     * 
     * @param name name of the node to search.
     * @param node node to search in.
     * @param maxdepth max search depth. 0 means to search direct childs of node only.
     * @return false if the search was interrupted.
     */
    public static List<Node> find(String name, Node node, int maxdepth) {
        final List<Node> result = new ArrayList<>();
        findRecur(name, node, maxdepth, item -> {
            result.add(item);
            return true;
        });
        return result;
    }

    /**
     * Search a node by name.
     * 
     * @param name name of the node to search.
     * @param node node to search in.
     * @param maxdepth max search depth. 0 means to search direct childs of node only.
     * @param callback called when an item is found.
     * @return false if the search was interrupted.
     */
    private static boolean findRecur(String name, Node node, int maxdepth, FindCallback callback) {
        if (!isElementNode(node)) {
            throw new XmlUtilsException("Error: Search node not of element type");
        }

        if (!node.hasChildNodes()) {
            return true;
        }

        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node subnode = list.item(i);
            if (isElementNode(subnode)) {
                if (subnode.getNodeName().equals(name) && !callback.item(subnode)) {
                    return false;
                }
                if (maxdepth > 0 && !findRecur(name, subnode, maxdepth - 1, callback)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean isElementNode(Node subnode) {
        return subnode.getNodeType() == Node.ELEMENT_NODE;
    }

    private interface FindCallback {
        /**
         * Called when a new item is found.
         * 
         * @param item Found node.
         * @return false to end the iteration.
         */
        boolean item(Node item);
    }

    private static class SingleItemCallback implements FindCallback {

        Node value;

        @Override
        public boolean item(Node node) {
            value = node;
            return false;
        }

    }

    public static class XmlUtilsException extends RuntimeException {
        private static final long serialVersionUID = 1117108744157468742L;

        public XmlUtilsException(String message) {
            super(message);
        }

        public XmlUtilsException(Throwable cause) {
            super(cause);
        }
    }
}
