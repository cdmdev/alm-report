package com.rios.alm.util;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

public final class DateUtils {

    private static Set<LocalDate> holydays;

    private DateUtils() {
    }

    static {
        try {
            holydays = new HashSet<>();
            Path path = Paths.get("holydays.txt");
            List<String> text = FileUtils.readLines(path.toFile());
            for (String line : text) {
                if (!StringUtils.isBlank(line) && !line.startsWith("#")) {
                    holydays.add(LocalDate.parse(line));
                }
            }
        } catch (IOException e) {
            LoggerFactory.getLogger(DateUtils.class).warn("", e);
        }
    }

    public static Set<LocalDate> getHolydays() {
        return holydays;
    }

    public static boolean isHolyday(LocalDate date) {
        return holydays.contains(date);
    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek weekday = date.getDayOfWeek();
        return weekday == DayOfWeek.SUNDAY || weekday == DayOfWeek.SATURDAY;
    }
}
