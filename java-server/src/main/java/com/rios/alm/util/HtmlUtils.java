package com.rios.alm.util;

import java.io.IOException;
import java.io.StringReader;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML.Attribute;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

import com.rios.alm.exception.SystemException;

public final class HtmlUtils {

    private static final class FormActionExtractor extends HTMLEditorKit.ParserCallback {
        private final String fieldName;
        private String lastAction;
        private String action;

        public String getAction() {
            return action;
        }

        private FormActionExtractor(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public void handleSimpleTag(Tag t, MutableAttributeSet a, int pos) {
            if (t.equals(Tag.INPUT) && fieldName.equalsIgnoreCase((String) a.getAttribute(Attribute.NAME))) {
                action = lastAction;
            }
        }

        @Override
        public void handleStartTag(Tag t, MutableAttributeSet a, int pos) {
            if (t.equals(Tag.FORM)) {
                lastAction = (String) a.getAttribute(Attribute.ACTION);
            }
        }

        @Override
        public void handleEndTag(Tag t, int pos) {
            if (t.equals(Tag.FORM)) {
                lastAction = null;
            }
        }
    }

    private HtmlUtils() {
        super();
    }

    /**
     * Extracts all the text in the html.
     * 
     * @param html
     *            Html to parse.
     * @return Text parsed.
     */
    public static String extractText(String html) {
        final ParserDelegator delegator = new ParserDelegator();
        final StringBuilder sb = new StringBuilder();
        try {
            // the third parameter is TRUE to ignore the charset
            delegator.parse(new StringReader(html), new HTMLEditorKit.ParserCallback() {
                @Override
                public void handleText(char[] data, int pos) {
                    sb.append(data);
                }

                @Override
                public void handleStartTag(Tag t, MutableAttributeSet a, int pos) {
                    if (t.breaksFlow() || t.isBlock()) {
                        sb.append(" ");
                    }
                }
            }, true);
        } catch (IOException e) {
            throw new SystemException(e);
        }
        // normalizing spaces
        return sb.toString().replaceAll("\\p{javaSpaceChar}+", " ").trim();
    }

    /**
     * Gets a form action attribute based in an input name.
     * 
     * @param html
     *            Html to parse.
     * @param fieldName
     *            Field name whose form action should be returned.
     * @return Form actino attribute or null.
     */
    public static String getFormUrl(String html, String fieldName) {
        final ParserDelegator delegator = new ParserDelegator();

        try {
            final FormActionExtractor formActionExtractor = new FormActionExtractor(fieldName);

            // the third parameter is TRUE to ignore the charset
            delegator.parse(new StringReader(html), formActionExtractor, true);

            return formActionExtractor.getAction();
        } catch (IOException e) {
            throw new SystemException(e);
        }
    }

}
