package com.rios.alm.util;

import java.util.Collections;
import java.util.List;

public final class OrderedList {

    private OrderedList() {

    }

    public static <E extends Comparable<E>> E insert(List<E> lst, E item) {
        int index = Collections.binarySearch(lst, item);
        if (index > 0) {
            return lst.get(index);
        }
        lst.add(-(index + 1), item);
        return item;
    }
}
