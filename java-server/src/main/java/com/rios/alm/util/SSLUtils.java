package com.rios.alm.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.rios.alm.exception.SystemException;

public final class SSLUtils {

    /**
     * Trust manager that does not validate certificate chains
     */
    private final static TrustManager[] TRUST_ALL_CERTS = new X509TrustManager[] {
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] {};
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    // does not verify
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    // does not verify
                }
            }
    };

    private SSLUtils() {
    }

    public static SSLContext createTrustAllSSLContext() {
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, TRUST_ALL_CERTS, new java.security.SecureRandom());

            return sc;

        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            throw new SystemException(e);
        }
    }
}
