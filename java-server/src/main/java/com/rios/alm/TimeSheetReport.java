package com.rios.alm;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import com.rios.alm.TimeSheetReport.DayVO;
import com.rios.alm.util.DateUtils;

public class TimeSheetReport extends ArrayList<DayVO> {

    private static final long serialVersionUID = 1043168092231138595L;

    private static final Locale PT_BR = new Locale("pt", "BR");

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("E d/MMM")
        .withLocale(PT_BR);

    private static final int MILLIS_IN_MINUTE = 60 * 1000;

    private static final double MILLIS_IN_MINUTE_D = (double) MILLIS_IN_MINUTE;

    private boolean formatTime;

    public TimeSheetReport(Map<LocalDate, DaySummary> daymap, Map<LocalDate, List<Absence>> absencemap,
            Map<LocalDate, Integer> timecard, LocalDate baseDate, int daysback, String username,
            boolean formatTime,
            boolean sumUstNotCompleted) {

        this.formatTime = formatTime;

        // monta o relatório

        LocalDate endDate = computeEndDate(daymap, baseDate);

        Context context = new Context();

        context.day = baseDate.minusDays(daysback);
        while (!context.day.isAfter(endDate)) {
            // gera o relatório do dia
            context.total = 0;
            final LocalDate day = context.day;

            DaySummary daySummary = daymap.get(day);

            List<WorkItemVO> workitems = buildWorkItems(username, daySummary, context);

            List<AbsenceVO> absences = buildAbsences(absencemap, context);

            DayVO dvo = new DayVO();
            dvo.desc = day.format(DATE_FORMAT);
            dvo.total = formatMillis(context.total);
            computeWorkRatio(timecard, dvo, context);
            dvo.workday = !DateUtils.isHolyday(day) && !DateUtils.isWeekend(day);
            dvo.absences = absences;
            dvo.workitems = workitems;
            dvo.future = daySummary != null && day.isAfter(baseDate);
            dvo.totalUst = workitems.stream()
                .filter(wi -> wi.ustItem != null && (wi.internalState.isCompleted() || sumUstNotCompleted))
                .mapToInt(wi -> wi.ustItem.getValue())
                .sum();
            this.add(dvo);

            context.day = day.plusDays(1);
        }
    }

    private LocalDate computeEndDate(Map<LocalDate, DaySummary> daymap, LocalDate baseDate) {
        LocalDate end = baseDate;
        for (Entry<LocalDate, DaySummary> entry : daymap.entrySet()) {
            LocalDate key = entry.getKey();
            if (key.isAfter(end)) {
                end = key;
            }
        }
        return end;
    }

    private void computeWorkRatio(Map<LocalDate, Integer> timecard, DayVO dvo, Context context) {
        dvo.worktotal = null;
        dvo.workratio = 0;
        if (timecard.containsKey(context.day)) {
            Integer workedMillis = timecard.get(context.day);
            dvo.worktotal = formatMillis(workedMillis);
            if (workedMillis != 0) {
                // trunca pra 3 casas depois da vírgula
                dvo.workratio =
                    ((int) ((context.total / MILLIS_IN_MINUTE_D / (workedMillis / MILLIS_IN_MINUTE_D))
                        * 1000)) / 1000.0d;
            }
        }
    }

    private List<WorkItemVO> buildWorkItems(String username, DaySummary daySummary, Context context) {
        List<WorkItemVO> workitems = new ArrayList<>();

        if (daySummary != null) {
            for (WorkItem wi : daySummary.getWorkitems()) {
                WorkItemVO wvo = new WorkItemVO();
                wvo.id = wi.getId();
                wvo.name = wi.getName();
                wvo.url = wi.getUrl();
                wvo.total = formatMillis(wi.daytotal(context.day, username));
                wvo.ustItem = wi.getUstItem();
                wvo.internalState = wi.getInternalState();
                workitems.add(wvo);
            }
            context.total = daySummary.getSum();
        }
        return workitems;
    }

    private List<AbsenceVO> buildAbsences(Map<LocalDate, List<Absence>> absencemap, Context context) {
        List<AbsenceVO> absences = new ArrayList<>();
        if (absencemap.containsKey(context.day)) {
            for (Absence absence : absencemap.get(context.day)) {
                AbsenceVO avo = new AbsenceVO();
                avo.name = absence.getDescription() != null ? absence.getDescription() : "Ausência";
                avo.total = String.format("%02d:00", absence.getHours());
                absences.add(avo);
                context.total = context.total + (absence.getHours() * 1000 * 60 * 60);
            }
        }
        return absences;
    }

    private String formatMillis(int millis) {
        if (millis == 0) {
            return "0";
        }
        double minutes = millis / (1000 * 60d);
        if (formatTime) {
            return String.format("%02d:%02d", (int) minutes / 60, (int) minutes % 60);
        }
        return String.format(PT_BR, "%.2f", minutes / 60d);
    }

    private static class Context {
        private int total;
        private LocalDate day;
    }

    public static class DayVO {

        public boolean future;
        public List<WorkItemVO> workitems;
        public List<AbsenceVO> absences;
        public boolean workday;
        public double workratio;
        public String worktotal;
        public String total;
        public String desc;
        public int totalUst;
    }

    public static class WorkItemVO {

        public InternalState internalState;
        public String total;
        public String url;
        public String name;
        public String id;
        public UstItem ustItem;
    }

    public static class AbsenceVO {

        public String total;
        public Object name;
    }
}
