package com.rios.alm;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rios.alm.util.OrderedList;

public class WorkItem implements Comparable<WorkItem> {
    private String id;
    private String name;
    private String url;
    private String modified;
    private boolean mine;
    private String type;
    private List<TimeSheetEntry> timesheet = new ArrayList<>();
    private List<WorkItem> children;
    private UstItem ustItem;
    private InternalState internalState;

    @SuppressWarnings("unused")
    private WorkItem() {
    }

    public WorkItem(String id, String url) {
        super();
        this.id = id;
        this.url = url;
    }

    public int daytotal(LocalDate date, String username) {
        int sum = 0;
        for (TimeSheetEntry tse : timesheet) {
            if (tse.getUsername().equals(username) && tse.getDate().equals(date)) {
                sum += tse.getSpent();
            }
        }
        return sum;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public List<TimeSheetEntry> getTimesheet() {
        return timesheet;
    }

    public void addChild(WorkItem child) {
        if (children == null) {
            children = new ArrayList<>(2);
        }
        OrderedList.insert(children, child);
    }

    public Collection<WorkItem> getChildren() {
        return children;
    }

    public boolean isMine() {
        return mine;
    }

    public void setMine(boolean mine) {
        this.mine = mine;
    }

    @Override
    @ParametersAreNonnullByDefault
    public int compareTo(WorkItem o) {
        return id.compareTo(o.getId());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkItem)) {
            return false;
        }
        return id.equals(((WorkItem) obj).getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getType() {
        return type;
    }

    protected boolean isStory() {
        return "story".equals(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModified() {
        return modified;
    }

    public boolean isUpToDate(String modified) {
        if (this.modified == null) {
            return false;
        }
        return !ZonedDateTime.parse(getModified()).isBefore(ZonedDateTime.parse(modified));
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public UstItem getUstItem() {
        return ustItem;
    }

    public void setUstItem(UstItem ustItem) {
        this.ustItem = ustItem;
    }

    public InternalState getInternalState() {
        return internalState;
    }

    public void setInternalState(InternalState internalState) {
        this.internalState = internalState;
    }

    @JsonIgnore
    public boolean isCompleted() {
        return internalState != null && internalState.isCompleted();
    }
}
