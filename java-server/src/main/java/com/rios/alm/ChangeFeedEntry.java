package com.rios.alm;

public class ChangeFeedEntry {

    private String summary;
    private String refTypes;
    private String updated;
    private String url;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getRefTypes() {
        return refTypes;
    }

    public void setRefTypes(String refTypes) {
        this.refTypes = refTypes;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
