package com.rios.alm;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.rios.alm.exception.LoginException;
import com.rios.alm.http.BasicHttpClient;
import com.rios.alm.http.LoggedInClient;
import com.rios.alm.util.XmlUtils;

public class AlmService extends LoggedInClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlmService.class);

    // 2007-05-21T14:50:32Z
    private static final DateTimeFormatter FEED_DATE_FORMAT =
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String URL_CCM = "https://alm.serpro/ccm/";
    private static final String URL_LOGIN = URL_CCM + "auth/j_security_check";
    private static final String URL_AUTH_FAILED = URL_CCM + "auth/authfailed";
    private static final String URL_AUTH_REQUIRED = URL_CCM + "auth/authrequired";
    private static final String URL_LOGOUT = URL_CCM
        + "service/com.ibm.team.repository.service.internal.ILogoutRestService";
    private static final String URL_SHOW_WORKITEM = URL_CCM
        + "resource/itemName/com.ibm.team.workitem.WorkItem/";
    private static final String URL_CCM_SERVICE = "https://alm.serpro/ccm/service/";

    private static final String URL_FEED_WORKITEM_CHANGES = URL_CCM_SERVICE
        + "com.ibm.team.repository.common.internal.IFeedService";
    private static final String URL_LISTPROJECTSCCM = URL_CCM_SERVICE
        + "com.ibm.team.process.internal.service.web.IProcessWebUIService/allProjectAreas";
    private static final String URL_GETWORKITEM = URL_CCM_SERVICE
        + "com.ibm.team.workitem.common.internal.rest.IWorkItemRestService/workItemDTO2";

    private static final BasicHttpClient CLIENT = makeHttpClient();

    public AlmService(String username, String password) {
        super(CLIENT, username, password);

    }

    private static BasicHttpClient makeHttpClient() {

        return new BasicHttpClient() {
            @Override
            protected void interceptResponse(HttpResponse response,
                HttpContext context) {
                Header[] location = response.getHeaders("location");
                if (location != null && location.length > 0
                    && (location[0].getValue().startsWith(URL_AUTH_FAILED)
                        || location[0].getValue().startsWith(URL_AUTH_REQUIRED))) {
                    throw new LoginException("Alm");
                }
                super.interceptResponse(response, context);
            }
        };
    }

    private RequestBuilder ajax(RequestBuilder rb) {
        rb.addHeader("X-com-ibm-team-configuration-versions", "LATEST");
        rb.addHeader("X-jazz-downstream-auth-client-level", "4.0");
        rb.addHeader("X-Requested-With", "XMLHttpRequest");
        // para forçar uma resposta em json: rb.addHeader("accept", "text/json")
        return rb;
    }

    public void loadWorkItemsOpenOrInProgress(Project project) {
        HttpUriRequest req = // buildGet("https://alm.serpro/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/727232")
            buildGet(String.format("https://alm.serpro/ccm/oslc/contexts/%s/workitems", project.getId()))
                .addHeader("OSLC-Core-Version", "2.0")
                .addHeader("Accept", "application/xml")
                .addParameter(
                    "oslc.where",
                    String.format("oslc_cm:closed=true and dcterms:contributor{foaf:nick=\"%s\"}", username))
                .addParameter("oslc.pageSize", "200")
                .addParameter(
                    "oslc.prefix",
                    "dcterms=<http://purl.org/dc/terms/>,rtc_cm=<http://jazz.net/xmlns/prod/jazz/rtc/cm/1.0/>,"
                        + "foaf=<http://xmlns.com/foaf/0.1/>,oslc_cm=<http://open-services.net/ns/cm#>")
                .addParameter(
                    "oslc.select",
                    "dcterms:title,dcterms:identifier,rtc_cm:type{rtc_cm:category},"
                        + "rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent{dcterms:title,dcterms:identifier,rtc_cm:type{rtc_cm:category}}")
                .build();
        LOGGER.debug(getText(req));
    }

    public String getWorkItemUrl(String wid) {
        return AlmService.URL_SHOW_WORKITEM + wid;
    }

    public List<Project> getProjects() {
        HttpUriRequest request = ajax(buildGet(URL_LISTPROJECTSCCM)).addParameter("userId", username).build();
        String xmltext = getText(request);
        Document doc = XmlUtils.createXmlDocument(xmltext);
        List<Project> result = new ArrayList<>();

        NodeList values = doc.getElementsByTagName("values");

        for (int i = 0; i < values.getLength(); i++) {
            Node value = values.item(i);
            if (XmlUtils.findSubNode("archived", value).getTextContent().equalsIgnoreCase("true")) {
                continue;
            }
            String pid = XmlUtils.findSubNode("itemId", value).getTextContent();
            String pname = XmlUtils.findSubNode("name", value).getTextContent();
            result.add(new Project(pid, pname));
        }

        return result;
    }

    public void logout() {
        EntityUtils.consumeQuietly(executeLoggedOff(buildPost(URL_LOGOUT).build()).getEntity());
        LOGGER.info("Logged out - {}", username);
        session.setLoginAlm(null);
    }

    public List<ChangeFeedEntry> getChanges(LocalDateTime startDate) {
        // documentação do serviço: https://jazz.net/wiki/bin/view/Main/FeedService
        HttpUriRequest request = ajax(buildGet(URL_FEED_WORKITEM_CHANGES))
            .addParameter("itemType", "WorkItem")
            .addParameter("maxResults", "400")
            .addParameter("author", username)
            .addParameter("since", startDate.format(FEED_DATE_FORMAT))
            .build();

        return parseChangeFeed(XmlUtils.createXmlDocument(getText(request)));
    }

    private List<ChangeFeedEntry> parseChangeFeed(Document doc) {
        NodeList entries = doc.getElementsByTagName("entry");

        List<ChangeFeedEntry> lst = new ArrayList<>(entries.getLength());

        for (int i = 0; i < entries.getLength(); i++) {

            // Entrada do feed no formato Atom.
            Node entry = entries.item(i);

            ChangeFeedEntry item = new ChangeFeedEntry();

            lst.add(item);

            item.setSummary(getTextContent(XmlUtils.first("summary", entry)));

            final Node refTypes = XmlUtils.first("syndication:reference_types", entry);
            item.setRefTypes(getTextContent(refTypes));

            final Node link = XmlUtils.first("link", entry);
            item.setUrl(getTextContent(link.getAttributes().getNamedItem("href")));

            item.setUpdated(getTextContent(XmlUtils.first("updated", entry)));
        }
        return lst;
    }

    private String getTextContent(Node node) {
        if (node != null) {
            return node.getTextContent();
        }
        return null;
    }

    public void loadWorkItem(String wid, WorkItem workItem) {
        LOGGER.info("Loading workitem {}", wid);

        /* TODO: usar o serviço:
         https://jazz.net/wiki/bin/view/Main/ResourceOrientedWorkItemAPIv2
         https://alm.serpro/ccm/oslc/workitems/727232.xml?oslc_cm.properties=dc:identifier,dc:title,rtc_cm:timeSheet{*{*,dc:creator{rtc_cm:userId}}} */

        HttpUriRequest request = ajax(buildGet(URL_GETWORKITEM)).addParameter("includeHistory", "false")
            .addParameter("id", wid)
            .build();
        String xmltext = getText(request);
        Document doc = XmlUtils.createXmlDocument(xmltext);
        Element root = doc.getDocumentElement();
        // pega o nome do workitem
        Node widto = XmlUtils.first("value", root);
        int attrsRemaining = 5;
        for (Node key : XmlUtils.find("key", widto, 2)) {
            String keyValue = key.getTextContent();
            attrsRemaining--;
            switch (keyValue) {
            case "summary":
                String content = getText("content", key.getParentNode());
                workItem.setName(StringEscapeUtils.unescapeHtml4(content));
                break;
            case "workItemType":
                String typeId = getText("id", key.getParentNode());
                // extrai o "com.ibm.team.apt.workItemType." do nome do tipo
                String[] typeparts = StringUtils.split(typeId, '.');
                typeId = typeparts[typeparts.length - 1];
                workItem.setType(typeId);
                break;
            case "modified":
                String modified = getText("id", key.getParentNode());
                workItem.setModified(modified);
                break;
            case "br.gov.serpro.alm.itemdetrabalho.atributo.itemust":
                String codeUstItem = getText("content", key.getParentNode());
                if (StringUtils.isNotBlank(codeUstItem)) {
                    codeUstItem = codeUstItem.split(" ")[0];
                }

                UstService ustService = new UstService();

                workItem.setUstItem(ustService.loadItem(codeUstItem));
                break;
            case "internalState":
                String internalState = getText("id", key.getParentNode());
                if (StringUtils.isNotBlank(internalState)) {
                    String[] splitState = internalState.split("\\.");
                    internalState = splitState.length > 0 ? splitState[splitState.length - 1] : "";
                }
                workItem.setInternalState(InternalState.fromCode(internalState));
                break;
            default:
                attrsRemaining++;
            }
            if (attrsRemaining == 0) {
                break;
            }
        }
        workItem.getTimesheet().clear();
        // gera o timesheet
        for (Node ts : XmlUtils.find("timeSpent", root, Integer.MAX_VALUE)) {
            Node entry = ts.getParentNode();
            String userId = getText("userId", entry).trim();
            int timeSpent = Integer.parseInt(ts.getTextContent());
            LocalDate tsDate = TimeSheetEntry.parseDate(getText("startDate", entry));
            TimeSheetEntry tse = new TimeSheetEntry(userId, tsDate, timeSpent);
            workItem.getTimesheet().add(tse);
        }
    }

    private String getText(String id, Node parentNode) {
        return XmlUtils.first(id, parentNode).getTextContent();
    }

    @Override
    protected void login() {
        if (session.getLoginAlm() != null) {
            LOGGER.info("Login requested. Last login: {}", session.getLoginAlm());
        }
        session.setLoginAlm(null);
        HttpUriRequest request = buildPost(URL_LOGIN)
            .addParameter("j_username", username)
            .addParameter("j_password", password)
            .build();
        EntityUtils.consumeQuietly(executeLoggedOff(request).getEntity());
        session.setLoginAlm(LocalDateTime.now());
    }

    @Override
    protected boolean isLoggedIn() {
        return session.getLoginAlm() != null;
    }

}
