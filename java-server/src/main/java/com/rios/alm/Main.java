package com.rios.alm;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rios.alm.rest.LastWeeksServlet;
import com.rios.alm.rest.WorkItemsServlet;

public class Main {
    private static Server server;

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static ThreadPool getThreadPool() {
        return server.getThreadPool();
    }

    public static void main(String[] args) throws Exception {

        int port;
        if (args.length > 0) {
            port = Integer.valueOf(args[0]);
        } else {
            port = 9090;
        }

        server = new Server(port);

        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setDirectoriesListed(true);
        resourceHandler.setEtags(true);
        resourceHandler.setWelcomeFiles(new String[]{"index.html" });

        resourceHandler.setResourceBase("static");
        LOG.info("serving {}", resourceHandler.getBaseResource());

        ServletHandler servletHandler = new ServletHandler();
        servletHandler.addServletWithMapping(LastWeeksServlet.class, "/rest/lastweeks");
        servletHandler.addServletWithMapping(WorkItemsServlet.class, "/rest/workitems");

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{resourceHandler, servletHandler, new DefaultHandler() });
        server.setHandler(handlers);

        server.start();
        server.join();
    }

}
