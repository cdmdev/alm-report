package com.rios.alm.http;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rios.alm.exception.RemoteServiceException;
import com.rios.alm.util.SSLUtils;

public class BasicHttpClient {

    public static final String DISABLE_RESPONSE_INTERCEPTOR_ATTR = "disable-interceptor";

    private static final int TIMEOUT = 15000; // 15s

    private final RequestConfig requestConfig;

    private final CloseableHttpClient client;

    private final Logger logger;

    private List<BasicHeader> defaultHeaders;

    public BasicHttpClient() {
        this(null);
    }

    public BasicHttpClient(List<BasicHeader> defaultHeaders) {
        this.defaultHeaders = defaultHeaders;
        requestConfig = makeRequestConfig().build();
        client = makeHttpClient().build();
        logger = LoggerFactory.getLogger(getClass());
    }

    protected RequestConfig.Builder makeRequestConfig() {
        return RequestConfig.custom()
            .setConnectionRequestTimeout(TIMEOUT)
            .setConnectTimeout(TIMEOUT)
            .setSocketTimeout(TIMEOUT)
            .setAuthenticationEnabled(false);
    }

    protected HttpClientBuilder makeHttpClient() {

        return HttpClients.custom()
            .addInterceptorLast((HttpResponseInterceptor) (response, context) -> {
                if (response.getStatusLine().getStatusCode() >= 400) {
                    interceptError(response, context);
                    return;
                }
                if (context.getAttribute(DISABLE_RESPONSE_INTERCEPTOR_ATTR) != Boolean.TRUE) {
                    interceptResponse(response, context);
                }
            })
            .setDefaultRequestConfig(requestConfig)
            .setRedirectStrategy(new LaxRedirectStrategy())
            .setSslcontext(SSLUtils.createTrustAllSSLContext())
            .disableConnectionState()
            .disableAuthCaching()
            .setDefaultHeaders(defaultHeaders)
            .setMaxConnPerRoute(20);
    }

    protected void interceptResponse(HttpResponse response, HttpContext context) {
        // pode ser utilizado pelas subclasses
    }

    protected void interceptError(HttpResponse response, HttpContext context) {
        logRemoteError(response);
        throw new RemoteServiceException(response.getStatusLine().getReasonPhrase());
    }

    protected void logRemoteError(HttpResponse response) {
        logger.warn("HTTP error: {}", response.getStatusLine());
    }

    public CloseableHttpResponse execute(HttpUriRequest request, HttpClientContext context) {
        try {
            return client.execute(request, context);
        } catch (IOException e) {
            throw new RemoteServiceException(e);
        }
    }
}
