package com.rios.alm.exception;

public class RemoteServiceException extends RuntimeException {

    private static final long serialVersionUID = 7926263198335723152L;

    public RemoteServiceException(String message) {
        super(message);
    }

    public RemoteServiceException(Throwable cause) {
        super(cause);
    }

    public RemoteServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
