package com.rios.alm.exception;

import java.text.MessageFormat;

public class LoginException extends RuntimeException {

    private static final long serialVersionUID = -3689550148037960665L;

    private static final String LOGIN_ERROR = "Não foi possível fazer login ({0}).";

    public LoginException(String source) {
        super(MessageFormat.format(LOGIN_ERROR, source));
    }
}
