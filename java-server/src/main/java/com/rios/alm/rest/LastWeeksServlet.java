package com.rios.alm.rest;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.rios.alm.Absence;
import com.rios.alm.AlmReport;
import com.rios.alm.DaySummary;
import com.rios.alm.EscritorioProjetos;
import com.rios.alm.Main;
import com.rios.alm.TimeSheetReport;
import com.rios.alm.exception.LoginException;
import com.rios.alm.exception.SystemException;
import com.rios.alm.timecard.TimeCardLoader;

@SuppressWarnings("serial")
public class LastWeeksServlet extends BaseHttpServlet {

    private static final ListenableFuture<Map<LocalDate, List<Absence>>> EMPTY_FUTURE_MAP = Futures
        .immediateFuture(Collections.emptyMap());

    private static final int MIN_DAYS_BACK = 21;

    private static final Logger LOGGER = LoggerFactory.getLogger(LastWeeksServlet.class);

    private static final boolean ESCRITORIO_PROJETOS_ENABLED = false;

    private static final TemporalAdjuster PREV_MONDAY_AJUSTER =
        TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY);

    private AjaxResponse lastWeeks(final String username, final String password, int page, boolean formatTime,
            boolean sumUstNotCompleted) {

        if (!RequestUtils.isUserAllowed(username)) {
            LOGGER.info("Access denied for user: {}", username);
            return new AjaxResponse(new AccessDeniedErrorDesc());
        }

        final AlmReport service = new AlmReport(username, password);

        Future<Map<LocalDate, List<Absence>>> futureAbsences;

        if (ESCRITORIO_PROJETOS_ENABLED) {
            futureAbsences = new FutureTask<>(() -> EscritorioProjetos.getInstance().getAbsences(username));
            Main.getThreadPool().execute((Runnable) futureAbsences);
        } else {
            futureAbsences = EMPTY_FUTURE_MAP;
        }

        // Calcula a data base e o número de dias
        LocalDate baseDate = LocalDate.now();
        if (page != 0) {
            // tira 2 dias pra mover pro sábado da semana passada
            baseDate = baseDate.with(PREV_MONDAY_AJUSTER).minusDays(page * MIN_DAYS_BACK + 2L);
        }
        int daysback = MIN_DAYS_BACK - 7 + baseDate.getDayOfWeek().getValue();
        if (daysback <= MIN_DAYS_BACK) {
            daysback += 7;
        }

        // inicia a thread para obter o ponto
        FutureTask<Map<LocalDate, Integer>> futureTimeCard = TimeCardLoader.getTimeCardAsync(
            username,
            password,
            baseDate,
            daysback);
        Main.getThreadPool().execute(futureTimeCard);

        Map<LocalDate, DaySummary> daymap = service.build(baseDate, daysback);

        Map<LocalDate, List<Absence>> absences = getAbsences(futureAbsences);
        // obtem o ponto
        Map<LocalDate, Integer> timecard = getTimeCard(futureTimeCard);
        
        // monta o relatório
        TimeSheetReport report = new TimeSheetReport(
            daymap,
            absences,
            timecard,
            baseDate,
            daysback,
            username,
            formatTime,
            sumUstNotCompleted);      
      
        return new AjaxResponse(new ResultResponse(report));
    }

    private Map<LocalDate, Integer> getTimeCard(FutureTask<Map<LocalDate, Integer>> futureTimeCard) {
        try {
            return futureTimeCard.get(15, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new SystemException(e);
        } catch (ExecutionException | TimeoutException e) {
            LOGGER.warn("Error loading time card.", e);
            return new HashMap<>();
        }
    }

    private Map<LocalDate, List<Absence>> getAbsences(Future<Map<LocalDate, List<Absence>>> futureAbsences) {
        // Tenta obter as ausências dentro de 1 segundo, senão retorna vazio
        try {
            return futureAbsences.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new SystemException(e);
        } catch (ExecutionException | TimeoutException e) {
            LOGGER.warn("Error loading absences.", e);
            return new HashMap<>();
        }
    }

    @Override
    protected void onPost(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("lastweeks - {}", RequestUtils.getRequestIp(req));
        String username = req.getParameter("user");
        String password = req.getParameter("password");
        int page = NumberUtils.toInt(req.getParameter("page"), 0);
        boolean formatTime = BooleanUtils
            .toBoolean(StringUtils.defaultString(req.getParameter("formatTime"), "true"));
        boolean sumUstNotCompleted = BooleanUtils
            .toBoolean(StringUtils.defaultString(req.getParameter("sumUstNotCompleted"), "true"));
        try {
            Object data = lastWeeks(username, password, page, formatTime, sumUstNotCompleted);
            RequestUtils.sendJson(resp, data);
        } catch (LoginException e) {
            RequestUtils.sendJson(resp, new AjaxResponse(new ErrorDesc(e.getMessage())));
        }
    }
}
