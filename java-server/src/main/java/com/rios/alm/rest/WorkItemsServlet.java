package com.rios.alm.rest;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rios.alm.AlmReport;
import com.rios.alm.Project;
import com.rios.alm.exception.LoginException;

@SuppressWarnings("serial")
public class WorkItemsServlet extends BaseHttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkItemsServlet.class);

    private AjaxResponse workItems(final String username, String password) {

        if (!RequestUtils.isUserAllowed(username)) {
            return new AjaxResponse(new AccessDeniedErrorDesc());
        }

        final AlmReport service = new AlmReport(username, password);

        Collection<Project> projects = service.getWorkItemsOpenOrInProgress();
        return new AjaxResponse(new ResultResponse(projects));
    }

    @Override
    protected void onPost(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("workitems - {}", RequestUtils.getRequestIp(req));
        String username = req.getParameter("user");
        String password = req.getParameter("password");
        try {
            Object data = workItems(username, password);
            RequestUtils.sendJson(resp, data);
        } catch (LoginException e) {
            RequestUtils.sendJson(resp, new AjaxResponse(new ErrorDesc(e.getMessage())));
        }
    }
}
