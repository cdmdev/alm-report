package com.rios.alm.rest;

public class AjaxResponse {

    private Object data;

    public AjaxResponse(Object data) {
        super();
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
