package com.rios.alm.rest;

public class ResultResponse {

    private Object result;

    public ResultResponse(Object result) {
        super();
        this.result = result;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

}
