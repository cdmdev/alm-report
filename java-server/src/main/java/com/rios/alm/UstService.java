package com.rios.alm;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import com.monitorjbl.xlsx.StreamingReader;
import com.rios.alm.db.JsonDB;

public class UstService {

    private final JsonDB<UstItem> ustDb;

    private final JsonDB<Boolean> configDb;

    public UstService() {
        ustDb = new JsonDB<>("ust:", UstItem.class);
        configDb = new JsonDB<>("config:", Boolean.class);

        loadCatalog();
    }

    public UstItem loadItem(String code) {
        return ustDb.load(code);
    }

    private void saveAll(List<UstItem> items) {
        items.forEach(ui -> ustDb.save(ui.getCode(), ui));

        configDb.save("catalogLoaded", true);
    }

    private void loadCatalog() {
        if (configDb.load("catalogLoaded") == null) {
            List<UstItem> ustItems = readFile(Paths.get("catalogo.xlsx"));

            saveAll(ustItems);
        }
    }

    private List<UstItem> readFile(Path path) {
        try (Workbook workbook = StreamingReader.builder()
            .rowCacheSize(100)
            .bufferSize(4096)
            .open(new FileInputStream(path.toFile()))) {

            List<UstItem> ustItems = new ArrayList<>();

            for (Row row : workbook.getSheetAt(0)) {
                String firstCell = row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue();
                if (hasData(firstCell)) {
                    UstItem ustItem = new UstItem();
                    ustItem.setCode(row.getCell(0).getStringCellValue());
                    ustItem.setValue(Float.valueOf(row.getCell(6).getStringCellValue()).intValue());

                    ustItems.add(ustItem);
                }
            }

            return ustItems;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private boolean hasData(String cellValue) {
        List<String> noData = Arrays.asList("Catálogo", "Grupo", "Categoria", "Mnemônico", "Obs");

        boolean withData = noData.stream()
            .filter(ld -> StringUtils.containsIgnoreCase(cellValue, ld))
            .count() == 0;

        return StringUtils.isNotBlank(cellValue) && withData;
    }
}
